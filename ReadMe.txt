These scripts are freeware and no warranty.

Intro:
======
These scripts are a tool for YouTube video's interactive subtitle. It
can convert the subtitle to TXT/LRC/SRT/SAA.

Usage:
======
1. Console Mode:
     python youtube_sub.py <-m|--mode subtitle_type> [subtitle] <out>
     -m|--mode: value can txt|lrc|srt|saa
2. GUI Mode: pythonw youtube.pyw
     This mode needn't you follow command-line parameter & save the
     subtitle to text first. Only need you copy->paste->convert.

Note:
======
This script using Python.Net for GUI, so your system must has the library.
