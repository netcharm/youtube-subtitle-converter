#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

#import locale
#import gettext
#
#rcdomain = 'youtube'
#localedir = os.path.realpath('locale')
#os.environ['LANG'] = locale.getdefaultlocale()[0]


from youtube_sub import *

#
# Using .net to create GUI
#
def CreateUI():
  import clr

  clr.AddReference('System.Windows.Forms')

  from System.Windows.Forms import Application

  from youtube_sub_ui import ConvertForm

  Application.EnableVisualStyles()
  form = ConvertForm()
  form.Show()
  Application.Run(form)

  pass

if __name__ == '__main__':
#  if os.path.exists(localedir):
#    gettext.bindtextdomain(rcdomain, localedir)
#    gettext.textdomain(rcdomain)
#    gettext.install(rcdomain, localedir, unicode=True)

  import win32api
  import win32console

  console = win32console.GetConsoleWindow()
  if console == 0:
    CreateUI()
  else:
    #Main()
    CreateUI()
    pass

