#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import io
import string
import codecs
import time
import datetime
import re
import getopt

rs = r'^\d+:\d+$'
re_src = re.compile(rs, re.I)

Style_Default_ENG = r'Style: Default,Tahoma,20,&H19000000,&H19843815,&H37A4F2F7,&HA0A6A6A8,0,0,0,0,100,100,0,0,1,2,2,2,10,10,10,1'
Style_Note_ENG = r'Style: Note,Times New Roman,22,&H19FFF907,&H19DC16C8,&H371E4454,&HA0969696,0,0,0,0,100,100,0,0,1,2,2,2,10,10,10,1'
Style_Title_ENG = r'Style: Title,Arial,28,&H190055FF,&H1948560E,&H37EAF196,&HA0969696,0,0,0,0,100,100,0,0,1,2,2,2,10,10,10,1'

Style_Default_CHS = r'Style: Default,微软雅黑,20,&H19000000,&H19843815,&H37A4F2F7,&HA0A6A6A8,0,0,0,0,100,100,0,0,1,2,2,2,10,10,10,1'
Style_Note_CHS = r'Style: Note,宋体,22,&H19FFF907,&H19DC16C8,&H371E4454,&HA0969696,0,0,0,0,100,100,0,0,1,2,2,2,10,10,10,1'
Style_Title_CHS = r'Style: Title,全真細隸書,28,&H190055FF,&H1948560E,&H37EAF196,&HA0969696,0,0,0,0,100,100,0,0,1,2,2,2,10,10,10,1'


def ReadFromText(title=None, contents=None):
  last_time = datetime.time(0, 0, 0)
  last_text = ''
  curr_time = datetime.time(0, 0, 0)
  curr_text = ''

  if contents != None:
    lines = []

    c = len(contents)

    for x in range(0, c):
      time = re_src.search(contents[x])
      if time != None:
        last_time = curr_time
        last_text = curr_text

        tt = format(u'%s' % (contents[x].replace('\n', ''))).split(':')
        tc = len(tt)
        if tc == 2:
          th = int(tt[0]) // 60
          tm = int(tt[0]) - th * 60
          ts = int(tt[1])
        elif tc == 3:
          th = int(tt[0])
          tm = int(tt[1])
          ts = int(tt[2])

        curr_time = datetime.time(th, tm, ts)
        curr_text = format(u'%s' % (contents[x+1].replace('\n', '')))

        lines.append([last_time, curr_time, last_text])

    th = curr_time.hour
    tm = curr_time.minute
    ts = curr_time.second

    dtt = datetime.datetime(1, 1, 1, th, tm, ts)
    dtt = dtt + datetime.timedelta(seconds=10)
    end_time = dtt.time()
    lines.append([curr_time, end_time, curr_text])

  return title, lines
  pass

def ReadFromFile(txt=None):
  title = None
  lines = None

  if txt != None:
    title = format(u'%s' % (os.path.splitext(txt)[0]))

    f = io.open(txt, 'r', encoding='utf-8')
    if f.readable() and f.seekable():
      l = f.readlines()
      title, lines = ReadFromText(title=title, contents=l)

    f.close()

  return title, lines
  pass

def ValidFileName(title, out):
  pos = out.find(title)
  path = out[:pos]

  fo = format('%s%s' % (path, title.replace('/', '_').replace(':', '_').replace('\\', '_').replace('|', '_')))
  return fo
  pass

def SaveToTXT(title=None, lines=None, out=None, bom=True):
  if (lines != None):
    fo = format('%s.txt' % (ValidFileName(title, out)))

    f = codecs.open(fo, 'w', encoding='utf-8')
    #f.write(str(codecs.BOM_UTF8, encoding='utf8'))

    lo = []
    if bom: 
      lo.append(str(codecs.BOM_UTF8, encoding='utf8'))

    for line in lines:
      lo.append(format(u'%s\n' % (line[0])))
      lo.append(format(u'%s\n' % (line[2])))

    f.writelines(lo)
    f.close()

  pass

def SaveToLRC(title=None, lines=None, out=None, bom=True):
  if (lines != None):
    fo = format('%s.lrc' % (ValidFileName(title, out)))

    f = codecs.open(fo, 'w', encoding='utf-8')
    #f.write(str(codecs.BOM_UTF8, encoding='utf8'))
    
    lo = []
    if bom: 
      lo.append(str(codecs.BOM_UTF8, encoding='utf8'))

    if title != None:
      lo.append(format(u'[ti]%s\n' % os.path.basename(title)))

    for line in lines:
      lo.append(format(u'[%s]%s\n' % (line[0], line[2])))

    f.writelines(lo)
    f.close()

  pass

def SaveToSRT(title=None, lines=None, out=None, bom=True):
  if (lines != None):
    #ft = os.path.splitext(out)[0]
    #fo = format('%s.srt' % (ft))
    fo = format('%s.srt' % (ValidFileName(title, out)))

    f = codecs.open(fo, 'w', encoding='utf-8')
    #f.write(str(codecs.BOM_UTF8, encoding='utf8'))

    id = 0
    lo = []
    if bom: 
      lo.append(str(codecs.BOM_UTF8, encoding='utf8'))

    for line in lines:
      id += 1
      ts = line[0].strftime('%H:%M:%S')
      te = line[1].strftime('%H:%M:%S')

      lo.append(format(u'%d\n' % (id)))
      lo.append(format(u'%s,000 --> %s,000\n' % (ts, te)))
      lo.append(format(u'%s\n' % (line[2])))
      lo.append(format(u'\n' % ()))

    f.writelines(lo)
    f.close()

  pass

def SaveToSSA(title=None, lines=None, out=None, bom=True):
  if (lines != None):
    #ft = os.path.splitext(out)[0]
    #fo = format('%s.ssa' % (ft))
    fo = format('%s.ssa' % (ValidFileName(title, out)))

    f = codecs.open(fo, 'w', encoding='utf-8')
    #f.write(str(codecs.BOM_UTF8, encoding='utf8'))

    lo = []
    if bom: 
      lo.append(str(codecs.BOM_UTF8, encoding='utf8'))
    
    lo.append(format(u"%s\n") % ('[Script Info]'))
    #lo.append(format(u"%s\n") % ('Title: '+os.path.basename(title)))
    lo.append(format(u"%s\n") % ('Title: '+ title))
    lo.append(format(u"%s\n") % (''))
    lo.append(format(u"%s\n") % ('[V4+ Styles]'))
    lo.append(format(u"%s\n") % ('Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding'))
    #lo.append(format(u"%s\n") % ('Style: Default,Tahoma,24,&H00FFFFFF,&HF0000000,&H00000000,&HF0000000,1,0,0,0,100,100,0,0.00,1,1,0,2,30,30,10,1'))
    lo.append(format(u"%s\n") % (Style_Default_CHS))
    lo.append(format(u"%s\n") % (Style_Note_CHS))
    lo.append(format(u"%s\n") % (Style_Title_CHS))
    lo.append(format(u"%s\n") % (''))
    lo.append(format(u"%s\n") % ('[Events]'))
    lo.append(format(u"%s\n") % ('Format: Layer, Start, End, Style, Actor, MarginL, MarginR, MarginV, Effect, Text'))

    for line in lines:
      ts = line[0].strftime('%H:%M:%S')
      te = line[1].strftime('%H:%M:%S')
      lo.append(format(u"Dialogue: 0,%s.00,%s.00,Default,NTP,0000,0000,0000,,%s\n") % (ts, te, line[2]))

    f.writelines(lo)
    f.close()

  pass


def Main():
  fi = None
  fo = None
  title = None
  lines = None
  mode = 'ssa'

  opts, srgs = getopt.getopt(sys.argv[1:], 'm:', ['mode='])
  #print(opts, srgs)
  for opt, value in opts:
    if opt.lower() in ('-m', '--mode'): mode = value.lower()

  argc = len(srgs)
  if argc == 0:
    print('Usage: ')
    print('==========================================================================')
    print('youtube_sub.py <-m|--mode subtitle_type> [youtube subtitle file] <outfile>')
    print('==========================================================================')
  else:
    if argc >= 1:
      fi = srgs[0]
    if argc >= 2:
      fo = srgs[1]
    elif fi != None:
      ft = os.path.splitext(fi)[1]
      fo = os.path.splitext(fi)[0]

    title, lines = ReadFromFile(fi)

    if mode == 'lrc':
      SaveToLRC(title, lines, fo)
    elif mode == 'txt':
      SaveToTXT(title, lines, fo)
    elif mode == 'ssa':
      SaveToSSA(title, lines, fo)
    elif mode == 'srt':
      SaveToSRT(title, lines, fo)

  pass

if __name__ == '__main__':
  Main()
