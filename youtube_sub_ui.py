﻿# -*- coding: utf-8 -*-

import clr
clr.AddReference('System')
clr.AddReference('System.Drawing')
clr.AddReference('System.Windows.Forms')

import System.Drawing
import System.Windows.Forms

from System.Drawing import *
from System.Windows.Forms import *

from System.Windows.Forms import Form
from System.Windows.Forms import MessageBox
from System.Windows.Forms import Keys
from System.Windows.Forms import Control

class ConvertForm(Form):
    def __init__(self):
        self.InitializeComponent()

    def InitializeComponent(self):
        self._edTitle = System.Windows.Forms.TextBox()
        self._edContents = System.Windows.Forms.TextBox()
        self._btnConvert = System.Windows.Forms.Button()
        self._grpSubType = System.Windows.Forms.GroupBox()
        self._btnSubTypeBOM = System.Windows.Forms.CheckBox()
        self._btnSubTypeSSA = System.Windows.Forms.RadioButton()
        self._btnSubTypeSRT = System.Windows.Forms.RadioButton()
        self._btnSubTypeLRC = System.Windows.Forms.RadioButton()
        self._btnSubTypeTXT = System.Windows.Forms.RadioButton()
        self._lblTitle = System.Windows.Forms.Label()
        self._lblContents = System.Windows.Forms.Label()
        self._btnBrowse = System.Windows.Forms.Button()
        self._status = System.Windows.Forms.StatusStrip()
        self._toolStripStatusLabel1 = System.Windows.Forms.ToolStripStatusLabel()
        self._dlgOpen = System.Windows.Forms.OpenFileDialog()
        self._lblPath = System.Windows.Forms.Label()
        self._edPath = System.Windows.Forms.TextBox()
        self._tray = System.Windows.Forms.NotifyIcon()
        self._dlgFolder = System.Windows.Forms.FolderBrowserDialog()
        self._grpSubType.SuspendLayout()
        self._status.SuspendLayout()
        self.SuspendLayout()
        #
        # edTitle
        #
        self._edTitle.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right
        self._edTitle.Location = System.Drawing.Point(89, 35)
        self._edTitle.Name = "edTitle"
        self._edTitle.Size = System.Drawing.Size(523, 21)
        self._edTitle.TabIndex = 0
        #
        # edContents
        #
        self._edContents.AcceptsReturn = True
        self._edContents.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right
        self._edContents.HideSelection = False
        self._edContents.Location = System.Drawing.Point(11, 86)
        self._edContents.MaxLength = 524288
        self._edContents.Multiline = True
        self._edContents.Name = "edContents"
        self._edContents.ScrollBars = System.Windows.Forms.ScrollBars.Both
        self._edContents.ShortcutsEnabled = True
        self._edContents.Size = System.Drawing.Size(508, 318)
        self._edContents.TabIndex = 1
        self._edContents.UseWaitCursor = False
        self._edContents.WordWrap = False
        self._edContents.KeyDown += self.EdContents_KeyDown
        #
        # btnConvert
        #
        self._btnConvert.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right
        self._btnConvert.Location = System.Drawing.Point(525, 381)
        self._btnConvert.Name = "btnConvert"
        self._btnConvert.Size = System.Drawing.Size(87, 23)
        self._btnConvert.TabIndex = 2
        self._btnConvert.Text = "Convert"
        self._btnConvert.UseVisualStyleBackColor = True
        self._btnConvert.Click += self.BtnConvert_Click
        #
        # grpSubType
        #
        self._grpSubType.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right
        self._grpSubType.Controls.Add(self._btnSubTypeTXT)
        self._grpSubType.Controls.Add(self._btnSubTypeLRC)
        self._grpSubType.Controls.Add(self._btnSubTypeSRT)
        self._grpSubType.Controls.Add(self._btnSubTypeSSA)
        self._grpSubType.Controls.Add(self._btnSubTypeBOM)
        self._grpSubType.Location = System.Drawing.Point(525, 203)
        self._grpSubType.Name = "grpSubType"
        self._grpSubType.Size = System.Drawing.Size(87, 172)
        self._grpSubType.TabIndex = 3
        self._grpSubType.TabStop = False
        self._grpSubType.Text = "Sub Type:"
        #
        # btnSubTypeBOM
        #
        self._btnSubTypeBOM.Checked = True
        self._btnSubTypeBOM.Location = System.Drawing.Point(14, 20)
        self._btnSubTypeBOM.Name = "btnSubTypeBOM"
        self._btnSubTypeBOM.Size = System.Drawing.Size(61, 24)
        self._btnSubTypeBOM.TabIndex = 0
        self._btnSubTypeBOM.TabStop = True
        self._btnSubTypeBOM.Text = "BOM"
        self._btnSubTypeBOM.UseVisualStyleBackColor = True
        #
        # btnSubTypeSSA
        #
        self._btnSubTypeSSA.Checked = True
        self._btnSubTypeSSA.Location = System.Drawing.Point(14, 50)
        self._btnSubTypeSSA.Name = "btnSubTypeSSA"
        self._btnSubTypeSSA.Size = System.Drawing.Size(61, 24)
        self._btnSubTypeSSA.TabIndex = 0
        self._btnSubTypeSSA.TabStop = True
        self._btnSubTypeSSA.Text = "SSA"
        self._btnSubTypeSSA.UseVisualStyleBackColor = True
        #
        # btnSubTypeSRT
        #
        self._btnSubTypeSRT.Location = System.Drawing.Point(14, 80)
        self._btnSubTypeSRT.Name = "btnSubTypeSRT"
        self._btnSubTypeSRT.Size = System.Drawing.Size(61, 24)
        self._btnSubTypeSRT.TabIndex = 1
        self._btnSubTypeSRT.Text = "SRT"
        self._btnSubTypeSRT.UseVisualStyleBackColor = True
        #
        # btnSubTypeLRC
        #
        self._btnSubTypeLRC.Location = System.Drawing.Point(14, 110)
        self._btnSubTypeLRC.Name = "btnSubTypeLRC"
        self._btnSubTypeLRC.Size = System.Drawing.Size(61, 24)
        self._btnSubTypeLRC.TabIndex = 2
        self._btnSubTypeLRC.Text = "LRC"
        self._btnSubTypeLRC.UseVisualStyleBackColor = True
        #
        # btnSubTypeTXT
        #
        self._btnSubTypeTXT.Location = System.Drawing.Point(14, 140)
        self._btnSubTypeTXT.Name = "btnSubTypeTXT"
        self._btnSubTypeTXT.Size = System.Drawing.Size(61, 24)
        self._btnSubTypeTXT.TabIndex = 3
        self._btnSubTypeTXT.Text = "TXT"
        self._btnSubTypeTXT.UseVisualStyleBackColor = True
        #
        # lblTitle
        #
        self._lblTitle.Location = System.Drawing.Point(11, 35)
        self._lblTitle.Name = "lblTitle"
        self._lblTitle.Size = System.Drawing.Size(72, 21)
        self._lblTitle.TabIndex = 4
        self._lblTitle.Text = "Title:"
        self._lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        #
        # lblContents
        #
        self._lblContents.Location = System.Drawing.Point(11, 60)
        self._lblContents.Name = "lblContents"
        self._lblContents.Size = System.Drawing.Size(105, 23)
        self._lblContents.TabIndex = 5
        self._lblContents.Text = "Contents:"
        self._lblContents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        #
        # btnBrowse
        #
        self._btnBrowse.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right
        self._btnBrowse.Location = System.Drawing.Point(525, 86)
        self._btnBrowse.Name = "btnBrowse"
        self._btnBrowse.Size = System.Drawing.Size(87, 23)
        self._btnBrowse.TabIndex = 6
        self._btnBrowse.Text = "Browse"
        self._btnBrowse.UseVisualStyleBackColor = True
        self._btnBrowse.Click += self.BtnBrowse_Click
        #
        # status
        #
        self._status.Items.AddRange(System.Array[System.Windows.Forms.ToolStripItem](
            [self._toolStripStatusLabel1]))
        self._status.Location = System.Drawing.Point(0, 420)
        self._status.Name = "status"
        self._status.Size = System.Drawing.Size(624, 22)
        self._status.TabIndex = 7
        #
        # toolStripStatusLabel1
        #
        self._toolStripStatusLabel1.Name = "toolStripStatusLabel1"
        self._toolStripStatusLabel1.Size = System.Drawing.Size(44, 17)
        self._toolStripStatusLabel1.Text = "Ready"
        #
        # dlgOpen
        #
        self._dlgOpen.DefaultExt = "*.txt"
        self._dlgOpen.FileName = "*.txt"
        self._dlgOpen.Filter = "Text File(*.txt)|*.txt|All files(*.*)|*.*"
        self._dlgOpen.ShowReadOnly = True
        self._dlgOpen.SupportMultiDottedExtensions = True
        self._dlgOpen.Title = "Open Text Subtitle contents file"
        #
        # lblPath
        #
        self._lblPath.Location = System.Drawing.Point(11, 8)
        self._lblPath.Name = "lblPath"
        self._lblPath.Size = System.Drawing.Size(72, 21)
        self._lblPath.TabIndex = 9
        self._lblPath.Text = "Folder:"
        self._lblPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        #
        # edPath
        #
        self._edPath.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right
        self._edPath.Location = System.Drawing.Point(89, 8)
        self._edPath.Name = "edPath"
        self._edPath.Size = System.Drawing.Size(523, 21)
        self._edPath.TabIndex = 8
        #
        # tray
        #
        self._tray.Text = "YouTube Subtitle Converter"
        self._tray.Visible = True
        self._tray.DoubleClick += self.Tray_DoubleClick
        #
        # dlgFolder
        #
        self._dlgFolder.ShowNewFolderButton = True
        #
        # ConvertForm
        #
        self.AcceptButton = self._btnBrowse
        self.ClientSize = System.Drawing.Size(624, 442)
        self.Controls.Add(self._lblPath)
        self.Controls.Add(self._edPath)
        self.Controls.Add(self._status)
        self.Controls.Add(self._btnBrowse)
        self.Controls.Add(self._lblContents)
        self.Controls.Add(self._lblTitle)
        self.Controls.Add(self._grpSubType)
        self.Controls.Add(self._btnConvert)
        self.Controls.Add(self._edContents)
        self.Controls.Add(self._edTitle)
        self.MinimumSize = System.Drawing.Size(400, 400)
        self.Name = "ConvertForm"
        self.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        self.Text = "YouTube Subtitle Converter"
        #self.Icon = System.Drawing.Icon("youtube_038.ico");
        self.FormClosing += self.ConvertForm_FormClosing
        self.Load += self.ConvertForm_Load
        self.SizeChanged += self.ConvertForm_SizeChanged
        self._grpSubType.ResumeLayout(False)
        self._status.ResumeLayout(False)
        self._status.PerformLayout()
        self.ResumeLayout(False)
        self.PerformLayout()

    def ConvertForm_Load(self, sender, e):
        AppName = os.path.abspath(sys.argv[0])
        AppPath = os.path.dirname(AppName)+"\\"

        #icon_file = System.IO.Path.Combine(AppPath, "youtube_038.ico")
        icon_file = os.path.join(AppPath, "youtube_038.ico")
        print(icon_file)
        self.Icon = System.Drawing.Icon(icon_file);
        self._tray.Icon = self.Icon

        self.lastWindowState = System.Windows.Forms.FormWindowState.Normal

        lastFolder, lastMode, lastBOM = self.LoadSetting()

        self._edPath.Text = lastFolder

        mode = lastMode.lower()
        if mode == 'ssa':
          self._btnSubTypeSSA.Checked = True
        elif mode == 'srt':
          self._btnSubTypeSRT.Checked = True
        elif mode == 'lrc':
          self._btnSubTypeLRC.Checked = True
        elif mode == 'txt':
          self._btnSubTypeTXT.Checked = True
        else:
          self._btnSubTypeSSA.Checked = True

        pass

    def ConvertForm_FormClosing(self, sender, e):
        self.SaveSetting()
        self._tray.Icon = None

    def ConvertForm_SizeChanged(self, sender, e):
        if self.WindowState == System.Windows.Forms.FormWindowState.Minimized:
            self.Hide()
        elif self.WindowState == System.Windows.Forms.FormWindowState.Maximized:
            self.lastWindowState = System.Windows.Forms.FormWindowState.Maximized
        else:
            self.lastWindowState = System.Windows.Forms.FormWindowState.Normal
        pass

    def Tray_DoubleClick(self, sender, e):
        self.Show()
        self.Activate()
        #self.WindowState = System.Windows.Forms.FormWindowState.Normal
        self.WindowState = self.lastWindowState
        pass

    def EdContents_KeyDown(self, sender, e):
        if (e.Modifiers == Keys.Control) and (e.KeyCode == Keys.A):
            sender.SelectAll()
            #sender.Paste()
            e.SuppressKeyPress = True;
        pass

    def BtnBrowse_Click(self, sender, e):
        #print(System.Environment.SpecialFolder.Destop)

        #self._dlgFolder.SelectedPath = self._edPath.Text
        #self._dlgFolder.ShowDialog()
        #self._edPath.Text = self._dlgFolder.SelectedPath
        pass

    def BtnConvert_Click(self, sender, e):
        mode = 'ssa'
        if self._btnSubTypeSSA.Checked:
          mode = 'ssa'
        elif self._btnSubTypeSRT.Checked:
          mode = 'srt'
        elif self._btnSubTypeLRC.Checked:
          mode = 'lrc'
        elif self._btnSubTypeTXT.Checked:
          mode = 'txt'
        else:
          mode = 'ssa'

        bom = True
        if self._btnSubTypeBOM.Checked:
          bom = True
        else:  
          bom = False
          
        title = self._edTitle.Text.strip()
        contents = self._edContents.Lines

        title, lines = ReadFromText(title=title, contents=contents)

        path = self._edPath.Text.strip()

        if (title != '') or (path != ''):
          out = (path+'\\'+title).replace('\\\\', '\\')

          if mode == 'lrc':
            SaveToLRC(title, lines, out, bom)
          elif mode == 'txt':
            SaveToTXT(title, lines, out, bom)
          elif mode == 'ssa':
            SaveToSSA(title, lines, out, bom)
          elif mode == 'srt':
            SaveToSRT(title, lines, out, bom)
          else:
            SaveToSSA(title, lines, out, bom)
        else:
          MessageBox.Show('No TITLE or PATH!', 'ERROR')

        pass

    def LoadSetting(self):
        AppName = os.path.abspath(sys.argv[0])
        AppPath = os.path.dirname(AppName)
        AppIni = os.path.splitext(AppName)[0] + '.ini'

        lastFolder = ''
        lastMode = 'ssa'
        lastBOM = True        

        if os.path.exists(AppIni):
          f = codecs.open(AppIni, 'r', encoding='utf-8')

          lines = f.readlines()

          for line in lines:
            text = line.lower()
            if text.find('lastfolder=') >= 0:
              lastFolder = text.replace('lastfolder=', '').strip()
            if text.find('lastmode=') >= 0:
              lastMode = text.replace('lastmode=', '').strip()
            if text.find('lastbom=') >= 0:
              lmode = text.replace('lastbom=', '').strip()
              if lmode.lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup']:
                lastBOM = True
              else:
                lastBOM = False

          f.close()
        return lastFolder, lastMode, lastBOM
        pass

    def SaveSetting(self):
        AppName = os.path.abspath(sys.argv[0])
        AppPath = os.path.dirname(AppName)
        AppIni = os.path.splitext(AppName)[0] + '.ini'

        lastFolder = self._edPath.Text.strip()

        if self._btnSubTypeSSA.Checked:
          lastMode = 'ssa'
        elif self._btnSubTypeSRT.Checked:
          lastMode = 'srt'
        elif self._btnSubTypeLRC.Checked:
          lastMode = 'lrc'
        elif self._btnSubTypeTXT.Checked:
          lastMode = 'txt'
        else:
          lastMode = 'ssa'

        if self._btnSubTypeBOM.Checked:
          lastBOM = True
        else:
          lastBOM = False
          
        f = codecs.open(AppIni, 'w', encoding='utf-8')

        if os.path.exists(lastFolder):
          f.write(format('%s=%s\n' % ('lastFolder', lastFolder)))
        f.write(format('%s=%s\n' % ('lastMode', lastMode)))
        f.write(format('%s=%s\n' % ('lastBOM', lastBOM)))

        f.close()
        pass


from youtube_sub import *

if __name__ == '__main__':

  from System.Windows.Forms import Application

  Application.EnableVisualStyles()
  form = ConvertForm()
  form.Show()
  Application.Run(form)

  pass
